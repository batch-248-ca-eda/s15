// console.log("Hello Wolrd!")
// mini activity print your name in the console
//F12 or Ctrl + Shift + C to access the console
//console.log("Japeth Christian E. Caneda")
// Cltrl / for comments

//Section: Syntax, Statements, and Comments

//Statement in programming are instructions that we tell the computer to perform
	// usually ends with the semicolon (;)
	// train us to locate where a statemend ends
	// alert("Hello World");
	// console.log("Hi");

// Syntax in programming, it is the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific manner to work.
// This is due to how theses codes were initially programmed to function and perform in a certain manner

// Where to place JavaScript
	// Inline - you can place JS right into the HTML page using the script tags
		// for very small sites and testing only
		// the online approach does not scale well, leads to poor organization, and code duplication
	// External - thisis a better approach
		// place JS into separate files and link to them from the HTML page
		// this approach is much easier to maintain, write and debug

// Use of Script Tag 
	// script tag can go anywhere on the page
	// as a best practice, many developers will plaec it just before the closing body tag on the HTML page.
	// This provides faster speed load times for our web page 

// we used devtools aalso to DEBUG, view messages and run JavaScript in the console tab

		console.log("Hello");
// whitespace can impact functionality in many ocmputer languages-BUT no in Javascript. In JS, whitespace is used only for readability and has no funcional impact. One effect of this is a single statement that can span multiple lines.

	console.log("Hello world1");

	console.log("Hello world2 " ) ;

	console.
	log
	(
		"Hello World3"
	);

// Comments
	// comments are parts of the code that gets ignored by the language
	// comments are meant to describbe the written code

		/*
			There are two types of commentsL
			1. The single-line comment denoted by two slashes (ctrl /)
			2. The multi-line comment denoted by a slash and asterisk (ctrl shift /)
		*/

// Variables
	// it is used to contain data
	// any information that is used by our applications are stored in what we call a memory
	// when we create variables, certain portions of a device's memory is given a "name" that we call "variable"

	// this makes it easier for us to associate information sotred in our devices to actual "names" about information

		let x = 1;
		let y;

	// Declaring variables 
		// Tells our devices that a variable name is create and is ready to store our data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined" meaning that the variable's value is "not defined"

	// Syntax
		//let/const variableName;
		//let/const variableNameOne;

	//let is a keyword that is usually used in declaring a variable

		let myVariable;

		//console.log(myVariable);
		//console.log() is useful ofr printing values of variables or certain results of code into the Google Chromes browsers console

		console.log(myVariable);//undefined

		//console.log(hello);//
		// variables must be declared first before they are used
		// using variables before they are declared will return an error
		let hello;

		/*
			Guidelines in writing variables;
			1. Use the 'let' keyword followed by the variables name of your choosing and us the assignment operator (=) to assign a value
			2. Variable names should start with a lowercase character, use camelCase for multiple words
			3. For Constant variables, use the 'const' keyword
			4 Variable names should be indicative or descriptive of the value being stored to avoid confusion

			Best practices in naming variables

			1.

			let firstName = "Michael";//good variable name
			let pokemon = "Charizard";//good variable name
			let pokemon = 25000;// bad variable name

			2.

			let FirstName = "Michael";//bad variable name
			let lastName = "Jordan";//good variable name

			3.

			let first name = "Mickey";//bad variable name
			let firstName = "Mickey";//good variable name

			lastName emailAddress mobileNumber internetAllowance

			let product_description = "cool product";


		*/

// Declaring and initializing variables
	// Initializing variables - the instance when a variable is given its initial/starting value

	// Syntax
		// let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 30000;
		console.log(productPrice);

		// In the context of certain applications, some vairables/information are constant and should not be changed
		// In this eample, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
		// This it the best way to prevent applications from suddenly breaking or performing in ways that are not intended 

		const interest = 3539;
